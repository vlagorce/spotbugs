#!/bin/bash -l

export ASDF_DATA_DIR="/opt/asdf"
export HOME=/root
. /root/.bashrc

########################################################
# Split a certificate bundle into multiple certificates
# Arguments:
#   location where all generated certificates are put
########################################################
function split_certificates(){
  cert_path="$1"
  mkdir $cert_path
  echo "$ADDITIONAL_CA_CERT_BUNDLE" | awk -v cert_path="$cert_path" 'BEGIN {x=0;} /BEGIN CERT/{x++} { print > cert_path "/custom." x ".crt"  }'
}

#########################################################
# Import multiple certificates to trust store
# Arguments:
#   Path of all certificates to be imported
#########################################################
function import_cert_to_trust_store(){
  cert_path="$1"
  keystore="$2"

  for file in ${cert_path}/*.crt; do
      if [ -f "$file" ]; then
          keytool -importcert -alias "$file"  \
                  -file "$file" \
                  -trustcacerts \
                  -noprompt -storepass changeit \
                  -keystore $keystore
      fi
  done
}

if [ -n "$ADDITIONAL_CA_CERT_BUNDLE" ]; then
  cert_path="/tmp/temp_certs"
  split_certificates $cert_path
  for keystore in $(find /opt/asdf/installs/java/ -name cacerts); do
    import_cert_to_trust_store $cert_path $keystore
  done
fi

/analyzer-binary "$@"
status=$?

# Move asdf tool versions files back
move_tool_versions_back

exit $status
